var express       = require('express');
var bodyParser    = require('body-parser');

//bot management
var botmgmt = require('./modules/bot_mgmt.js');

//password file
var passwd = require('./json/passwd.json');

//session
var session = require('./modules/session.js');

//setting express
var app = express();

//Using body-parser
app.use(bodyParser.urlencoded({extended:true}));

//engine
app.engine('html', require('ejs').renderFile);
app.use(express.static('./views', {hidden: true}));

//sessionId付与
app.get('/genssid', function(req, res) {
   res.contentType('application/json');
   var ssid = session.genRandStr();
   var salt = session.genRandStr();
   session.createHash(ssid, passwd.passwd, salt);
   var json = JSON.stringify({
       "ssid" : ssid,
       "salt" : salt
   });
   res.send(json);
});

//コンソールページ
app.get('/console', function(req, res) {
    res.render('console.html');
});

//botの起動
app.get('/start', function(req, res) {
    //post元の正当性確認
    if(session.isTrustClient(req.query.ssid, req.query.hash)) {
        //start bot
        botmgmt.start();
        //res.send(botmgmt.getStatus())
        res.send('bot start successfull');
    } else {
        res.send("Bad authorication");
    }
});

//botの停止
app.get('/stop', function(req, res) {
    //post元の正当性確認
    if(session.isTrustClient(req.query.ssid, req.query.hash)) {
        //stop bot
        botmgmt.stop();
        res.send('bot stop successfull');
    } else {
        res.send("Bad authorication");
    }
});

//botの再起動
app.get('/restart', function(req, res) {
    //post元の正当性確認
    if(session.isTrustClient(req.query.ssid, req.query.hash)) {
        //restart bot
        botmgmt.restart();
        res.send('bot restart successfull');
    } else {
        res.send("Bad authorication");
    }
});

//botのステータス確認
app.get('/status', function(req, res) {
    if(session.isTrustClient(req.query.ssid, req.query.hash)) {
        //show status
        res.contentType('application/json');
        res.send(botmgmt.getStatus());
    } else {
        res.send("Bad authorication");
    }
});

//代理ツイート
app.get('/tweet', function(req, res) {
    if(session.isTrustClient(req.query.ssid, req.query.hash)) {
        res.send('tweet success');
        botmgmt.tweet(req.query.tweetText);
    } else {
        res.send("Bad authorication");
    }
});

//DB再読み込み
app.get('/reloaddb', function(req, res) {
    if(session.isTrustClient(req.query.ssid, req.query.hash)) {
        botmgmt.reloadDb(req.query.table);
    } else {
        res.send("Bad authorication");
    }
});

app.listen(3001);
