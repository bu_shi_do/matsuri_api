var crypto = require('crypto');

var hashes = {};

function hash(string) {
    var md5hesh  =crypto.createHash('md5');
    md5hesh.update(string, 'binary');
    return md5hesh.digest('hex');
}

function isTrustClient(sessionId, hashed) {
    var result  = (hashes[sessionId] === hashed);
    hashes[sessionId] = null;
    return result;
}

function createHash(sessionId, passwdHash, salt) {
    var hashStr = hash(passwdHash + salt);
    hashes[sessionId] = hashStr;
}

function genRandStr() {
    return Math.random().toString(36).slice(-8);
}

module.exports = {
    isTrustClient : isTrustClient,
    createHash    : createHash,
    genRandStr    : genRandStr,
    hash          : hash
};
