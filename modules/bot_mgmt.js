var child_process = require('child_process');

var twitter = require('../../matsuri_bot/self_modules/twitter.js');

var routineBus, routineProject, autoReply;

function start() {
    if(!routineBus)         routineBus = child_process.fork('/home/matsuri_project/matsuri_bot/school_bus_bot/routine.js');
    if(!routineProject) routineProject = child_process.fork('/home/matsuri_project/matsuri_bot/event_alert/event_alert.js');
    if(!autoReply)           autoReply = child_process.fork('/home/matsuri_project/matsuri_bot/reply/reply_main.js');
}

function stop() {
    var botStatus = getStatus();
    if(botStatus.isRoutineBusAlive) routineBus.send({ message : "stop" });
    if(botStatus.isRoutineProjectAlive) routineProject.send({ message : "stop" });
    if(botStatus.isAutoReplyAlive) autoReply.send({ message : "stop" });
}

function restart() {
    stop();
    setTimeout(start, 1000);
}

function getStatus() {
    return {
        isRoutineBusAlive     : (routineBus.exitCode === null),
        isRoutineProjectAlive : (routineProject.exitCode === null),
        isAutoReplyAlive      : (autoReply.exitCode === null)
    };
}

function reloadDb(table) {
}

function tweet(twText) {
    if(twText.indexOf('#kit_festa_i')===-1) twText += '\n#kit_festa_i';
    twitter.tw(twText);
}

module.exports = {
    start     : start,
    stop      : stop,
    restart   : restart,
    getStatus : getStatus,
    tweet     : tweet
};
