var fs = require('fs');
var crypto = require('crypto');

var passwdFile = ('../json/passwd.json');

var passwdData = { passwd : ''};

if(process.argv.length<4) {
    console.log("This command usage: node passwd-manager.js chpass <passwd>");
    process.exit();
}

var command = process.argv[2];
var param   = process.argv[3];

var md5hesh = crypto.createHash('md5');
md5hesh.update(param, 'binary');
var password = md5hesh.digest('hex');

function registPasswd(passwd) {
    passwdData.passwd = passwd;
    fs.writeFile(passwdFile, JSON.stringify(passwdData), function(err) {
        if(err) {
        console.log(err);
        } else {
            console.log("passwd write success!");
        }
    });
}

fs.readFile(passwdFile, 'utf8', function(err, data) {
    if(err) console.log(err);
    if(data.length === 0) {
        console.log("null passwd file. prease type passwd");
        registPasswd();
    }
    passwdData = JSON.parse(data);
});

switch(command) {
    case 'chpass':
        registPasswd(password);
        break;
    default:
        console.log("eligal args Usage: chpass [password]");
        process.exit(1);
        break;
}

module.exports = {
    passwd : passwdData.passwd,
    registPasswd : registPasswd
};
